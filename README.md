# Automação de Teste com Cypress

A aplicação de uso para esse projeto é o site **Open Trivia Database**, pode ser acessado pela seguinte url :point_down:
> https://opentdb.com/

### Projeto

O Projeto esta estruturado da seguinte maneira:

```shell
cypress
├── fixtures
│   └── user.json
├── integration
│   ├── common
│   ├── Login
│   │   └── Login.feature
│   └── Search
│       └── Search.feature
├── plugins
│   └── index.js
└── support
    ├── commands.js
    ├── index.js
    ├── page
    │   ├── Login
    │   │   ├── elements.js
    │   │   └── index.js
    │   └── Search
    │       ├── elements.js
    │       └── index.js
    └── steps
        ├── Login
        │   └── steps.js
        └── Search
            └── steps.js 
```

1. **Fixture** é onde se encontra a massa de teste em arquivos .json, porém nesse projeto não foi utilizado.
2. **Integration** é onde estamos armazenando as _features_ com nossa escreta **Gherkin**.
3. **Plugins** é onde está as configurações de plugins de terceiro como _cucumber_ e _mochawesome_.
4. **Support** é onde deixamos armazenado nossos _Steps_ e também nossa estrutura de _PageObjects_.
5. **Reports** esse diretório somente irá existir após a execução dos teste. É importante excluir esse diretório após cada execução, apenas como garantia que será criado um relatório do teste atual. Para exclusão, digite:
> npm run cy:cls

## Configurações

Os teste podem ser executados de 3 formas, irem descrever da mais simples para a mais trablhosa.

### GitLab-CI
___

No repositório do **GitLab**, se direcione para **_CI/CD > pipelines_** e rode as pipelines disponiveis:
>>>
  * smoke
  * login
  * search
  * all
>>>

1. **smoke**: refere-se aos teste definido com importantes
2. **login**: refere-se aos teste de login
3. **search**: refere-se aos teste de busca por questões
4. **all**: executa todos os testes


### Docker && Docker Compose
___

Importante para execução dos testes, tenha as ferramentas abaixo intaladas em seu computador :point_down:
>>>
  * docker && docker-compose
>>>

1. Para contruir a imagem:
> docker-compose build

2. Para subir o Container:
> docker-compose up -d

3. Para rodar todos os dados:
> docker-compose run e2e npm run cy:all

4. Para rodar apenas os teste de smoke:
> docker-compose run e2e npm run cy:smoke

5. Para rodar apenas os teste de login:
> docker-compose run e2e npm run cy:login

6. Para rodar apenas os teste de search:
> docker-compose run e2e npm run cy:search

7. Para matar o Container
> docker-compose down


### NodeJS && NPM
___

Para essa configuração é importante a instalação do NodeJS e NPM. Após isso esteja na raiz do projeto e digite os comandos:
>>>
  * npm install
>>>

1. Para rodar todos os dados:
> npm run cy:all

2. Para rodar apenas os teste de smoke:
> npm run cy:smoke

3. Para rodar apenas os teste de login:
> npm run cy:login

4. Para rodar apenas os teste de search:
> npm run cy:search 

### Reports
___

O diretório e arquivo index.html serão criados apartir de cada teste executados, então poderá ser aberto em um navagador.

```shell
cypress
└── reports
    ├── index.html
```
