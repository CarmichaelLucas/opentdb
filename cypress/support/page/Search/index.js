/// <reference types="cypress" />

import searchElements from "./elements";

class SearchPage {

  setup() {
    cy.get(searchElements.btnBrowse())
    .click();
  }

  querySearch(query) {
    cy.get(searchElements.inputQuery())
      .type(query);
  }

  goToSearch() {
    cy.get(searchElements.btnSearch())
      .click()
  }

  optionTypeQuery(type) {
    cy.get(searchElements.selectType())
      .select(type);
  }

  validateListQuestion(quantity) {
    cy.get('tbody')
      .children('tr')
      .should('have.length', quantity)
  }
  validatePaginationControl() {
    cy.get('ul.pagination').should('be.visible');
  }

  searchQuestionFail(message) {
    cy.get(searchElements.divAlert())
      .should('have.text', message);
  }
}

export default new SearchPage;