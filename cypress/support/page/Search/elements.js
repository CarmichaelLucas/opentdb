class SearchElements {
  btnBrowse  = () => { return '.nav > :nth-child(1) > a' }
  inputQuery = () => { return '#query' }
  selectType = () => { return '#type' }
  btnSearch  = () => { return 'div button.btn' }
  divAlert   = () => { return 'div.alert' }
}

export default new SearchElements;