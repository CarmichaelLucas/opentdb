/// <reference types="cypress" />

import loginElements from './elements';


class LoginPage {
  setup() {
    cy.get(loginElements.navBar())
      .contains('Login')
      .click()
  }

  handlingElements(username, password) {
    cy.get(loginElements.inputUserName())
      .clear()
      .type(username);

    cy.get(loginElements.inputPassWord())
      .clear()
      .type(password);
  }

  goToLogin() {
    cy.get(loginElements.btnSubmit())
      .click();
  }

  validateLoginSuccess(username) {
    cy.get(loginElements.navBar())
      .should('contain', username); 
  }

  validateLoginFail(message) {
    cy.get(loginElements.messageError())
      .should('contain', message);
  }
};

export default new LoginPage;

