class LoginElements {
  navBar          = () => { return 'div#navbar ul li a' }
  inputUserName   = () => { return '#username' }
  inputPassWord   = () => { return '#password' }
  btnSubmit       = () => { return 'button[type=submit]' }
  titleLogin      = () => { return 'h2' }
  messageError    = () => { return '.alert' }
};

export default new LoginElements;