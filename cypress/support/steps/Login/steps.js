/* global Given, And, Then, When */

import loginPage from '../../page/Login';


Given("que acesso a pagina de Login", () => {
  loginPage.setup();
});

And("informo username {string} com password {string}", (username, password) => {
  loginPage.handlingElements(username, password);
});

And("informo {string} com {string}", (username, password) => {
  loginPage.handlingElements(username, password);
});

When("clico em fazer login", () => {
  loginPage.goToLogin();
});

Then("deve exibir username {string} no menu", (username) => {
  loginPage.validateLoginSuccess(username);
});

Then("deve exibir {string}", (message) => {
  loginPage.validateLoginFail(message);
});