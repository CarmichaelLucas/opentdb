/* global Given, And, Then, When */

import searchQuestion from '../../page/Search';


Given("que navego para a página de busca do banco de questões", () => {
  searchQuestion.setup();
});

And("digito {string} no campo de busca", (query) => {
  searchQuestion.querySearch(query);
});

When("seleciono tipo por {string}", (type) => {
  searchQuestion.optionTypeQuery(type);
});

And("clico no botão de buscar", () => {
   searchQuestion.goToSearch();
});

Then("visualizo listagem com {int} itens", (quantity) => {
  searchQuestion.validateListQuestion(quantity);
});
 
And("exibe o controle de paginação", () => {
  searchQuestion.validatePaginationControl();
});

Then("visualizo uma mensagem de erro com o texto {string}", (message) => {
  searchQuestion.searchQuestionFail(message);
});