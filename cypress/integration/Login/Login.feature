#language: pt
Funcionalidade: Login
  
  Contexto:
    Dado que acesso a pagina de Login
  
  @smoketest @login
  Cenario: Realizar Login com sucesso
    E informo username "123lucas" com password "123456"
    Quando clico em fazer login
    Então deve exibir username "123lucas" no menu

  @login
  Esquema do Cenario: Realizar Login sem sucesso
    E informo "<username>" com "<password>"
    Quando clico em fazer login
    Então deve exibir "<mensagem>" 

    Exemplos:
    | username  | password  | mensagem            |
    | 123lucas  | 987654    | Logging In Failed.  |
    | 321lucas  | 654321    | Logging In Failed.  |