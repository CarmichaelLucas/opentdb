#language: pt
Funcionalidade: Busca no Banco de Questões

  Contexto: 
    Dado que navego para a página de busca do banco de questões
  
  @smoketest @search
  Cenario: Busca por questão existente
    E digito "Science: Computers" no campo de busca
    Quando seleciono tipo por "Category" 
    E clico no botão de buscar 
    Entao visualizo listagem com 25 itens
    E exibe o controle de paginação
  
  @search
  Cenario: Busca por questão inexistente  
    E digito "Science: Computers" no campo de busca
    Quando seleciono tipo por "Question" 
    E clico no botão de buscar 
    Entao visualizo uma mensagem de erro com o texto "No questions found."